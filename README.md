# cs-preliminaries

學習資訊工程的先期準備。

## 參考

- [APCS](https://apcs.csie.ntnu.edu.tw/) 大學程式設計先修檢測
- [ZeroJudge](https://zerojudge.tw/) 高中生程式解題系統

## 說明

- 內容應該視為概念性的學習重點。
- 每一種主題都可以加深、擴大，作為往後的研修方向，而不僅限於文字內容。
