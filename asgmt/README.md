# 練功房

1. [CodeSignal](https://codesignal.com/)

   美國軟體公司 CodeSignal，建立遊戲機制提供各類線上程式挑戰。

1. [LeetCode](https://leetcode.com/)

   收集軟體工程師面試考古題的線上練功網站。

1. [Kaggle](https://www.kaggle.com/)

   建模和數據分析競賽平台。

1. [Programiz](https://www.programiz.com/)

   包含模擬編譯器的線上學習平台。
