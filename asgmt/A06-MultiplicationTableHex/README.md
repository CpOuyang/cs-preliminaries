# Multiplication Table (Hex)

列出十六進位乘法表，如下圖。

```batch
1x1= 1  1x2= 2  1x3= 3  1x4= 4  1x5= 5  1x6= 6  1x7= 7  1x8= 8  1x9= 9  1xA= A  1xB= B  1xC= C  1xD= D  1xE= E  1xF= F  
2x1= 2  2x2= 4  2x3= 6  2x4= 8  2x5= A  2x6= C  2x7= E  2x8=10  2x9=12  2xA=14  2xB=16  2xC=18  2xD=1A  2xE=1C  2xF=1E  
3x1= 3  3x2= 6  3x3= 9  3x4= C  3x5= F  3x6=12  3x7=15  3x8=18  3x9=1B  3xA=1E  3xB=21  3xC=24  3xD=27  3xE=2A  3xF=2D  
4x1= 4  4x2= 8  4x3= C  4x4=10  4x5=14  4x6=18  4x7=1C  4x8=20  4x9=24  4xA=28  4xB=2C  4xC=30  4xD=34  4xE=38  4xF=3C  
5x1= 5  5x2= A  5x3= F  5x4=14  5x5=19  5x6=1E  5x7=23  5x8=28  5x9=2D  5xA=32  5xB=37  5xC=3C  5xD=41  5xE=46  5xF=4B  
6x1= 6  6x2= C  6x3=12  6x4=18  6x5=1E  6x6=24  6x7=2A  6x8=30  6x9=36  6xA=3C  6xB=42  6xC=48  6xD=4E  6xE=54  6xF=5A  
7x1= 7  7x2= E  7x3=15  7x4=1C  7x5=23  7x6=2A  7x7=31  7x8=38  7x9=3F  7xA=46  7xB=4D  7xC=54  7xD=5B  7xE=62  7xF=69  
8x1= 8  8x2=10  8x3=18  8x4=20  8x5=28  8x6=30  8x7=38  8x8=40  8x9=48  8xA=50  8xB=58  8xC=60  8xD=68  8xE=70  8xF=78  
9x1= 9  9x2=12  9x3=1B  9x4=24  9x5=2D  9x6=36  9x7=3F  9x8=48  9x9=51  9xA=5A  9xB=63  9xC=6C  9xD=75  9xE=7E  9xF=87  
Ax1= A  Ax2=14  Ax3=1E  Ax4=28  Ax5=32  Ax6=3C  Ax7=46  Ax8=50  Ax9=5A  AxA=64  AxB=6E  AxC=78  AxD=82  AxE=8C  AxF=96  
Bx1= B  Bx2=16  Bx3=21  Bx4=2C  Bx5=37  Bx6=42  Bx7=4D  Bx8=58  Bx9=63  BxA=6E  BxB=79  BxC=84  BxD=8F  BxE=9A  BxF=A5  
Cx1= C  Cx2=18  Cx3=24  Cx4=30  Cx5=3C  Cx6=48  Cx7=54  Cx8=60  Cx9=6C  CxA=78  CxB=84  CxC=90  CxD=9C  CxE=A8  CxF=B4  
Dx1= D  Dx2=1A  Dx3=27  Dx4=34  Dx5=41  Dx6=4E  Dx7=5B  Dx8=68  Dx9=75  DxA=82  DxB=8F  DxC=9C  DxD=A9  DxE=B6  DxF=C3  
Ex1= E  Ex2=1C  Ex3=2A  Ex4=38  Ex5=46  Ex6=54  Ex7=62  Ex8=70  Ex9=7E  ExA=8C  ExB=9A  ExC=A8  ExD=B6  ExE=C4  ExF=D2  
Fx1= F  Fx2=1E  Fx3=2D  Fx4=3C  Fx5=4B  Fx6=5A  Fx7=69  Fx8=78  Fx9=87  FxA=96  FxB=A5  FxC=B4  FxD=C3  FxE=D2  FxF=E1  
```

\# for <br>\# str
