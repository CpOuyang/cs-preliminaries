def print_tree(n, c):
    i = 0
    while i < n:
        space = ' ' * (n - i)
        star = c * (i*2 + 1)
        line = space + star
        print(line)
        i = i + 1
