# Print A Tree

在 console 畫面上用星號 (*) 列印出一棵樹，如下圖。

```batch
    *
   ***
  *****
 *******
*********
```

\# for <br>\# in
