# 開場

Python 是 1991 年才初版問世的直譯式語言，在此之前的主流語言，如C/C++ 等，功能雖然強大，都有篇幅過長、學習與檻高的特性。

因此 Python 修改與簡化了許多當代主流語言的缺點，但在效能上也有所犧牲。[[參考]](https://zh.wikipedia.org/wiki/Python)

- 規範程式碼的排版。
- 強調程式碼的可讀性。
- 簡化複雜的結構與定義方式。
- 強調語言的泛用性。
- 確保程式在簡化後的可運作，需要許多看不見的檢核與除錯。

近年的硬體技術進步的帶動下，Python 在執行速度上的缺點獲得彌補，其彈性與簡潔在開放社群有很好的發揮，帶動許多**數據應用**與**人工智慧**的發展。

## 語法 Syntax

Python 的語法規範。以下只摘出最基本的重點，以便開始上手。

1. 完全捨棄 `tab` 字元。

1. 只使用 `space` 作間隔。

1. 捨棄大括號 ({ }) 作為函數、迴圈的界線。

1. 使用 4 個 `space` 為一單位的『推進』(indentation)，作為函數、迴圈的界線。

1. 使用『換行』(line break)，作為函數間的界線。

1. 避免行末端 (trailing) 的 `space`。

完整的語言規範，官方推出一系列的文章，稱作 Python Enhancement Proposals ([PEP](https://www.python.org/dev/peps/))，針對各版本的各種語言特性，詳細說明了開發者心目中的想法。

其中，上述的書寫規範，被列在第八篇，故稱為 PEP8。任何符合標準的 Python 程式，都必須遵守 [PEP8](https://www.python.org/dev/peps/pep-0008/)。

## 禪 Zen

程式碼的簡潔化，在複雜的專案中扮演很重要的功能。Python 將規範的宗旨，引用中文的禪意，內藏在各版本中，推廣至世界。

```
>>> import this
The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
```
