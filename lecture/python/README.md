# Python

[[_TOC_]]

內建的指令與物件，請參考 3.X [官方文件](https://docs.python.org/3/index.html)，餘參考專案文件。

## Fundation

- [x] [Intro](materials/Intro)
- [x] [PEP](https://www.python.org/dev/peps/)
- [ ] [pip](https://pip.pypa.io/)
- [ ] [PyPI](https://pypi.org/)

## Built-in

### Operator

- [ ] `+` `-` `*` `/`
- [ ] `+=` `-=` `*=` `/=`
- [ ] `or` `and` `not` `is` `in`
- [ ] `|` `^` `&`
- [ ] `//` `%`
- [ ] `**`
- [ ] `>` `<` `==` `>=` `<=` `!=`

### Data Type

- [ ] `None`
- [ ] `bool`
- [ ] `bytes`
- [ ] `int`
- [ ] `float`
- [ ] `complex`
- [ ] `str`
- [ ] `tuple`
- [ ] `list`
- [ ] `dict`

### Syntax

- [ ] Comments
- [ ] Documents
- [ ] Function
- [ ] Iterator
- [ ] Class `class`
  - [ ] Property
  - [ ] Method
  - [ ] Class Method
- [ ] Simple Statements
  - [ ] `import`
  - [ ] `assert`
  - [ ] `raise`
  - [ ] `with`
- [ ] Compound Statements
  - [ ] `if` ... [`elif` ...] [`else` ...]
  - [ ] `for` ... [`else` ...]
  - [ ] `while` ... [`else` ...]
  - [ ] `until` ... [`else` ...]
- [ ] Decorators
- [ ] Exceptions
  - [ ] `try` ... `except` [... [`as` ...]]<br>[`else` ...] [`finally` ...]
- [ ] Lambda

### Function

- [ ] `help`
- [ ] `dir`
- [ ] `type`
- [ ] `isinstance`
- [ ] `id`
- [ ] `any`
- [ ] `all`
- [ ] `range`
- [ ] `open`
- [x] `bin`, `oct`, `hex`

Refer to offical: [built-in functions](https://docs.python.org/3/library/functions.html).

### Class

- [ ] `object`
- [ ] Property
- [ ] Method
- [ ] Instance Method ([Data Model](https://docs.python.org/3/reference/datamodel.html))
  <!-- - [ ] `__name__` -->
  - [ ] `__init__`
  - [ ] `__repr__`
  - [ ] `__class__`
  - [ ] `__len__`
- [ ] Class Method

### Module

- [ ] `typing` ([PEP484](https://www.python.org/dev/peps/pep-0484/))
- [ ] `datetime`
- [ ] `os`
- [ ] `sys`
- [ ] `re`

---

- [ ] `unittest`
- [ ] `logging`
- [ ] `argparse`

## Third Party

- [ ] virtualenv
- [ ] pyenv
- [ ] Anaconda
- [ ] Miniconda

---

- [ ] Requests
- [ ] Scrapy
- [ ] BeautifulSoup
- [ ] NumPy
- [ ] Pandas

### Framework

- [ ] Jupyter
- [ ] Flask
- [ ] Django

### Deep Learning (A.I.)

- [ ] TensorFlow
- [ ] Keras
- [ ] PyTorch
