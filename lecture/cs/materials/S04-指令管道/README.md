# 指令管道 (Command Pipe)

在一個行程 (process) 執行期間，可以連續的、一個接著一個的，完成一連串指令，這種串接方式，彷彿管線 (Pipeline) 一樣串接，簡稱為管道 (Pipe)。

```batch
> command1 | command2 | command3
```

分解動作：

1. command1 讀取其 stdin；
1. command1 的 stdout 交予 command2 成為其 stdin；
1. command2 的 stdout 交予 command3 成為其 stdin；
1. command3 產出其 stdout

## 實例

```batch
:: 查詢可用指令，並一頁一頁翻看
> help | more
```

```batch
:: 查詢目前 IP
> ipconfig | findstr IPv4
```

```batch
:: 列出 chrome 的 processes，依照記憶體大至小(降冪)排序
> tasklist | findstr chrome | sort /r /+40
```

理解了 pipe 的應用，再配合大量的指令，就可以玩出很多技巧。需要多看多練習。
