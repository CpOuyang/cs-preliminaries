# 殼語言 (Shell Language)

殼語言是一種腳本語言，是作業系統上搜集資訊，與下達指令最直接的語言。又稱為殼腳本 (Shell Script)。

## 核心 (Kernel) vs. 殼層 (Shell)

一般抽象的概念，會將系統的核心稱為 kernel，則殼語言 (Shell Language) 即是指包在外層，像是『殼』似的抽象層，在核心外圍與使用者互動，故名。

換言之，Shell Lang. 是指令列的直譯器 (interpreter)，指令的執行者，提供對應的指令服務。

### Unix Shell

Unix 系列對 shell 發展相當成熟且多元，差異在特定功能上的支援。

- sh
- bash

以上二種是最廣被熟知。

- csh
- tcsh
- ash
- zsh
- fish
- and more ...

Unix 系列選一種殼語言精熟就可以了。

### Windows Shell

Windows 家族的殼語言選擇並不多。

- cmd.exe
- PowerShell.exe

## 定位

殼語言的定位，始終就圍繞在系統核心，所以目的不是在打造華麗花俏的功能，而是：

1. 搜集系統資訊
1. 設定系統環境
1. 下達系統指令
1. 系統排程 (Scheduling)
1. 批次作業 (Batched jobs)

強調的都是對機器的溝通與使用。

## 參考

[[ss64]](https://ss64.com/) 網站將常見作業系統的殼語言，以及歷代版本做了不錯的整理，還有補充範例。
