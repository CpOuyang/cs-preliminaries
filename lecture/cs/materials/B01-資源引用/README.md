# 資源引用

以程式開發功能，是疊床架屋的過程，經常需要外部資源以加速達成。這裡所指的外部，是指專案以外。

隨著語言的不同與時代的演進，通常有以下幾種樣態。

## 函式庫 (Library)

預寫好的可擴展功能。可以支援不同的程式以提供服務。(早期)函式庫通常有以下二種區分。

### 靜態函式庫

多個函式庫，由編譯器藉由連結器組裝。

- 沒有條件或動態的選用，所以通常比較肥大。

### 動態函式庫

在程式啟動的當下，動態的視需求去引用函式庫。

- Winodws 常見的 *.dll，就是依不同功能開發出的函式庫。

## 框架 (Framework)

除了預先開發出功能，還會加入抽象 (abstract) 的定義或架構，稱為框架。

- Microsoft .Net Framework
- MVC Pattern

## 模組 (Module)

針對某領域，而預先開發出完整功能的程式集。

## 語言包 (Package)

針對現有開發，而選取模組下特定功能的集合。

## 軟體開發套件 (Software Development Kit, SDK)

大型的專案會將相關的函式庫或框架，及相關開發工具打包為應用程式，讓更多開發者安裝、開發及散佈，稱為 SDK。

- .Net SDK
- DirectX SDK
- iOS SDK
- Android SDK

## Java

甲骨文推出的 Java Development Kit 稱為 JDK。

Java 除了版本好，還分為標準版 SE 與企業版 EE，差別就是在函式庫的多寡。安裝 JDK 時還需在注意這部分。

## Python

以 Python 為例，

- 官方推出的[標準函式庫](https://docs.python.org/3/library/index.html) (Standard Library)。
- 以檔案或資料夾方式組建出不同功能的模組 (Module)。
- 程式碼針對模組內挑選所需的語言包 (Package)。

在專案開發時，不會將標準函式庫全部引用，而是打散成 modules 或 packages 各自取用。
