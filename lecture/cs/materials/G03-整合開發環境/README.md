# 整合開發環境 (IDE)

輔助開發者的整合開發環境 (Integrated Development Environment, IDE)，可以加速開發，保持良好習慣。

## 演進

1. 早期的程式編輯，依賴的工具就是文字編輯器。
1. 某些 Linux 的編輯工具，如 vim、emacs，在程式設計師很專注的插件 (Plugins) 幫助下，可以改出適用的開發環境。
   - 需要很大量的插件。
   - 記憶很多熱鍵指令。
   - 堅持對程式的熱忱。
   - 硬核工程師的象徵。

   有興趣再延伸討論。

## 其他

早期的 IDE 產品，多半都專注在單一語言的開發環境。

1. 軟體公司 Borland [[參考]](https://zh.wikipedia.org/wiki/Borland)
   - Turbo Pascal
   - Turbo C++
   - JBuilder
   - C++ Builder
   - Delphi (整合型)
   - ...

1. 軟體公司 Adobe
   - Dreamweaver (網頁)

   Adobe 是家視覺設計產品公司，少數還堅持推出網頁編輯器產品。以往的競爭者幾乎都已經退出市場。

爾後陸續推出支援多語言開發版本。

1. Sublime Text
   - 跨平台的文字編輯器。
   - 多種套件支援各種語言的編輯。

1. SharpDevelop
   - C# / Visual Basic 等 .Net 框架。

1. Eclipse
   - 早先 IBM 旗下產品，2001 Nov. 貢獻開源。
   - 早期專注 Java，後加入 C / C++ / PHP / COBOL 等。

1. NetBeans
   - Sun Microsystems 昇陽電腦產品。
   - 支援 Java / C / C++ / PHP / HTML5 等。

1. Atom
   - Github 推出的開源跨平台 IDE。
   - 功能類似 vscode，但受歡迎程度已不如前。

## 推薦

1. Notepad++ [[參考]](https://notepad-plus-plus.org/)
   - 文字編輯器的常見替代品。
   - OS: Windows

1. JetBrains [[參考]](https://www.jetbrains.com/)
   - 推出相當多當代語言 IDE 的軟體公司。
   - 包含專案管理、團隊管理等工具。
   - 總部捷克布拉格。
   - 多是付費產品，Python 有推出社群版本 (free)。

1. Microsoft

   - 早期 (<2000) 微軟針對軟體開發，推出一系列 Visual 產品
      - Visual C++
      - Visual J++
      - Visual Basic
      - ...

   - 2002 .Net Framework 推出後，Visual 家族都是整合型版本，近年開始支援跨平台。[[參考]](https://visualstudio.microsoft.com/)
      - Visual Studio 2022
      - Visual Studio 2022 Community

   - 微軟為了打入開源社群，獲取更多開發者，2015 推出免費的開源版本。近年成為相當受歡迎的開源跨平台 IDE。
      - Visual Studio Code [[參考]](https://code.visualstudio.com/)
