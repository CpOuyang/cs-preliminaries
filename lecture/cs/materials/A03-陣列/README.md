# 陣列 (Array)

連續數個同質或異質的值，相互索引串聯，形成的物件，稱為陣列。

## 概念

實作方法即是在記憶體紀錄每一個值的同時，紀錄下一個值的位址。相關領域稱為**資料結構** (**Data Structure**)，衍伸出很多演算法問題。測驗很常考，故點出。

```mermaid
classDiagram
direction LR
class Element1 {
    Value1
    (address2)
}
class Element2 {
    Value2
    (address3)
}
class Element3 {
    Value3
    (...)
}
Element1 --> Element2: Link
Element2 --> Element3: Link
```

陣列還有很多種相似形，如堆疊 (Stack)、佇列 (Queue)、樹 (Tree) 等。

以 Python 為例

```python
array = [1, 2, 3, 4, 5]
```

## Index

通常，陣列會設計下標 (index)，以方便帶出對應的元素。

值得一提的是，通常 CS 領域的下標，習慣以 $`0`$ 為起始。如 $`0`$, $`1`$, $`2`$, ...。

```python
>>> # Python
>>> array = [1, 2, 3, 4, 5]
>>> print(array[0])
1
```

## 矩陣 (Matrix)

數學上，還可以將更多數值元素放在二個維度上表示，稱為矩陣 (Matrix)。例如，

```math
mat=
\begin{bmatrix}
    1 & 2 & 3 \\
    2 & 4 & 6 \\
    3 & 6 & 9
\end{bmatrix}
```

如果要自行實作，就是以『陣列的陣列』(array of arrays) 來處理。例如，

```python
mat = [[1, 2, 3], [2, 4, 6], [3, 6, 9]]
```

再實作出**表達**與**下標**的方法，方可使用。

若要操作更高維度的抽象物件，就再以陣列方式往下擴展即可。
