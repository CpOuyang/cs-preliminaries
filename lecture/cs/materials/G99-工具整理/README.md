# 工具整理 (Toolings)

工欲善其事，必先利其器。

## Console

| Name | Desc |
| :- | :- |
| Chocolatey | |

## IDE

| Name | Desc |
| :- | :- |
| Notepad++ | 以 C++ 開發的文字編輯器，專用於 Windows 作業系統。 |
| UltraEdit | 功能強大、模組眾多的**商業化**文字編輯軟體。 |
| PyCharm Community | JetBrain 公司推出的 Python IDE 社群版。 |
| Vim | Linux 的內建編輯器，功能強大，指令快捷鍵經常會被其他程式引用。 |
| Visual Studio Code | |
| Visual Studio Community | |

## Program

| Name | Desc |
| :- | :- |
| ILSpy | .Net 的反編譯器。可以檢視 .Net 框架的組成。 |

## Network

| Name | Desc |
| :- | :- |
| ngrok | |
| Postman | |
