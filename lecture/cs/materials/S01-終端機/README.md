# 終端機 (Terminal)

多數的計算機都會提供介面與使用者溝通，而溝通最有效的方式，便是使用指令。[[參考]](https://www.geeksforgeeks.org/difference-between-terminal-console-shell-and-command-line/)

## 終端 Terminal

系統與使用者溝通的端口。通常有通常視窗有圖像化介面、儀表板，或指令列。

## 系統控制台 (System) Console

系統資源彙整，與使用者溝通的終端介面。

- 遊戲開發商推出的遊戲主機，也稱作 console。 

## 指令列介面 Command-line Interface (CLI)

互動式視窗，提供指令輸入，與其執行結果。

- CLI 是終端的其中一視窗界面。有時也稱 CLI 為 Console。
