# 標準串流 (Standard Streams)

想像一種資訊的內部流動，從輸入裝置（例如鍵盤）開始，進入到應用程式（例如 shell），執行完畢後結果輸出到終端設備（例如螢幕）；這種抽象的資訊流，稱為標準串流。

標準串流經常著重在輸入裝置 (Input) 與輸出裝置 (Output)，又稱為 Standard I/O。

標準串流可分為三種類型。

```mermaid
flowchart TB;
    keyboard -->|#0 stdin| process
    process -->|#1 stdout| display
    process -->|#2 stderr| display
    subgraph Terminal
    keyboard
    display
    end
```

## 標準輸入 Stdin

任何一段程式要執行，都需要輸入源，例如：要 小畫家 開啟某份圖檔。

- 預設的 stdin 是：鍵盤。
- stdin 的定向代碼：0

## 標準輸出 Stdout

- 預設的 stdout 是：螢幕。
- stdout 的定向代碼：1

## 標準錯誤輸出 Stderr

程式總會出錯，不管是程式本身，或是工程師的問題。錯誤的輸出通常會與正常執行完成的產出不同。

- 預設的 stderr 是：螢幕。
- stderr 的定向代碼：2

## 重導向符號 ">"

> 注意以下範例中，開頭的 ">" 是 cmd 命令列的提示字元 (prompt)，而不是導向符號。

[Win/Unix] `echo` 指令功能：將其後的字串輸出至 stdout。

```batch
> echo 123
123
```

可以使用 ">" 導至指定檔案。

```batch
> echo 123 > my_text.txt
```

例如我們有一段批次指令碼要執行。

> 雙冒號 (::) 是 cmd 的單行註解，
> 表示其以後的所有文字，都是使用者註解，不是程式。
> 不同語言會有其不同的定義方式。

```batch
:: 此例中 cmd.exe /c 可以不寫，讓副檔名(*.bat)去產生關聯。
> cmd.exe /c jobs.bat
```

可以依照自己的需要，加以調整。

```batch
:: 執行 jobs.bat，將結果除存在 result.txt，如果出錯，訊息存在 error.txt
> cmd.exe /c jobs.bat 1> result.txt 2> error.txt
```

導向符號">"之後要接定向代碼，要使用跳脫 (escape) 字元"&"。

```batch
:: 執行 jobs.bat，錯誤的訊息 (stderr) 請丟到螢幕 (stdout) 上
> cmd.exe /c jobs.bat 2>&1
```

## 重導向符號 ">>"

使用 ">>" 以附加方式 (append) 導至指定檔案，而不是覆寫 (overwrite)。

```batch
> echo 123 >> my_text.txt
```

通常保存 log 的方式都會用附加方式導向，以長期觀察。

## 重導向符號 "<"

就是 ">" 符號的反向。將 "<" 右方的檔案內容，傳至左方成為其 stdin。

```batch
:: 以下二種執行結果雖然相同，但是意涵不同。

:: 呼叫 python，請執行 jobs.py
> python jobs.py

:: jobs.py 的內容，請交予 python 的 stdin 來執行
> python < jobs.py
```

> 請注意，**沒有** "<<" 符號。

## 黑洞 [Win]"nul" / [Unix]"/dev/null"

輸出的資訊我不想理會，程式請不要處理訊息，連螢幕上都不想看到。

```batch
> echo 123 > nul
```

注意這個用法不是系統不執行程式，而是完成執行後或當下，資訊不想看到。

## 結語

標準串流的應用效果十分強大，是**黑客**標準配備，需要多加練習。

自此以後，我們可以將任何出現在螢幕上的資訊加以留存。

參考 [[ss64]](https://ss64.com/nt/syntax-redirection.html)。