# 作業系統 (Operating System)

作業系統，是介於硬體層，與應用程式之間的系統軟體，負責硬體資源的配置與管理，與應用程式的請求回應、錯誤處理等。

```mermaid
flowchart TB;
    n1[[使用者 Users]] <--> n2[[應用程式 Applications]]
    n2[[應用程式 Applications]] <--> n3[[作業系統 Operating System]]
    n3[[作業系統 Operating System]] <--> n4[[硬體 Hardware]]
```

## Unix 家族

Unix 作業系統自 1970s 由貝爾實驗室研發推出，在自由授權的條件下獲得相當多的改版 [[參考]](https://en.wikipedia.org/wiki/Unix-like)。包含商業化或非商業化的版本，且各有各自的支持者。例如自由開發的貢獻者，多會以 Linux 作為開發基礎。

- Linux
  - ubuntu
  - RedHat
  - SUSE
- BSD
- 企業化:
  - macOS
  - SunOS
  - AIX
  - Solaris
  - Chrome OS
  - Android

以上僅列出代表名稱，光是 Linux 本身就很很多種版本。[[參考]](https://en.wikipedia.org/wiki/Comparison_of_Linux_distributions)

舉凡各種終端、手機、平板、主機等，都會以 unix 家族為基本去改款。

1. 修改容易
1. 使用者眾
1. 資源豐沛

受到很多使用者支持。

## Windows 系列

1985 後，Microsoft 進攻個人電腦(PC)市場，視窗介面的作業系統獲得大眾的熟悉與歡迎。以下僅列非伺服器版本的代表作。

- Windows 3.x / 9X
- Windows XP / Vista / 7 / 8 / 8.1
- Windows CE

以上已停止支援。

- Windows 10
- Windows 11 (未上市)
- Xbox OS

## 指令集架構 (ISA)

### 32-bit vs. 64-bit

常見到作業系統區分為 32-bit 版本、64-bit 版本，主要是在說明該作業系統採取的**指令集架構** (Instruction Set Architecture, ISA)。[[參考]](https://en.wikipedia.org/wiki/Instruction_set_architecture)

2000s 後期，逐漸無法承受 32-bit 作業系統的指令集數量緊繃，後繼的作業系統逐漸採用 64-bit 架構。

- 以下是 MIPS 定義的『整數加法』指令 `addi`。[[參考]](https://zh.wikipedia.org/wiki/MIPS%E6%9E%B6%E6%A7%8B)

<img src="img/01.png" alt="drawing" width="400"/>

- $`2^6=64`$
- $`2^{32}=4,294,967,296`$
- $`2^{64}=18,446,744,073,709,551,616`$

### x86 系統

x86 是指 1978 年起 Intel 推出的一系列處理器，依照 ISA 的演進，分別有 80186、80286、80386、80486等，因皆為 32-bit 以下的架構，統稱 x86。

故連同往後幾代的 Pentium 處理器，32-bit ISA 架構泛稱為 x86 系統。
