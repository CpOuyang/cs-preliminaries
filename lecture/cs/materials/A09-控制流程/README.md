# 控制流程 (Control Flow)

程式碼逐行推進，可以依據條件不同而給予特別的行進方式。[[參考]](https://en.wikipedia.org/wiki/Control_flow)

1. 下面各節列出一般常見的控制流程，並不表示程式語言都會全數包含。

1. 程式碼用**虛擬代碼** (**Pseudo Code**) 來演示。實際上如何執行，要視選用的程式語言如何實現。

## Label + GOTO

```text
LABEL:

...

GOTO LABEL
```

> `GOTO` 是最直觀且比較早期語法，但容易產生臭蟲且不易維護，在近代語言中多半都被移除。

## IF

```text
IF condition THEN

    logic

END-IF
```

## IF-ELSE

```text
IF condition-1 THEN

    logic-1

ELSE IF condition-2 THEN

    logic-2

ELSE

    logic-n

END-IF
```

## SWITCH-CASE

```text
SWITCH target

    CASE value-1

        logic-1

    CASE value-2

        logic-2

    ...

END-SWITCH
```

## FOR Loop

```text
FOR counter-in-index DO

    logic

END-DO
```

`counter-in-index` 是指以某個變數開始計數，超過某範圍後停止。

以 GNU C++ 為例

```c++
for (i = 1; i = i + 1; i < 100) {
    cout << i;
}
```

## WHILE Loop

```text
WHILE condition-is-true DO

    logic

END-WHILE
```

## UNTIL Loop

```text
UNTIL condition-is-true DO

    logic

END-UNTIL
```

## FOR-EACH Loop

```text
FOR EACH element-in-collection DO

    logic

END-DO
```

`counter-in-collection` 表示以某個集合的元素來列舉。

以 C# 為例

```c#
Span<int> numbers = new int[] { 3, 14, 15, 92, 6 };
foreach (int number in numbers) {
    Console.Write($"{number} ");
}
// Output:
// 3 14 15 92 6
```
