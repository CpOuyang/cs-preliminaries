# 函數 (Function)

將運算方法，包裝成一個可重複呼叫、運算的實體，是為函數。

**計算機**領域的函數，強調的重點與**數學**領域有所不同。以下個人觀點。

|  | 數學領域 | 計算機領域 |
|:-|:-|:-|
| 重點 | 輸入 (input) 與輸出 (output) 的關係，以及函式的特性可以帶來哪些影響。 | 輸入 (input) 與輸出 (output) 的傳遞，以及不同物件、數量下的計算方法。 |
| 輸入 | 一個或多個數字 | 一個或多個數字、文字，或任何具體或抽象的物件。 |
| 輸出 | 一個數字，但強調連續性的關係。 | 一個或多個數字、文字，或任何具體或抽象的物件。 |

## 命名與邏輯

舉個簡單例子。

```python
# Python
def func(s):

    print(s)
```

從上述來看，一個(計算機領域的)函數大抵都包含：

- 第一部分 (`def ...`)
  - 定義函數名稱。
  - 定義函數接收變數的方式。

- 第二部分 (`print ...`)
  - 定義函數邏輯 (包含回傳結果)。
  - 定義結束界線。

## 簽名 (Signature)

> 函數接收變數的方式，稱作函數的**簽名** (signature)。

任何函數在每次使用時，都要依照最初定義的簽名進行。

```text
>>> func('Hello World!')
Hello World!
```

不依簽名規範執行函數，會產生相對應的錯誤。

```text
>>> func()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: func() missing 1 required positional argument: 's'

>>> func(1, 2)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: func() takes 1 positional argument but 2 were given
```

> 函數的輸入變數可以都不定義，這也是一種簽名。

沒有輸入變數的函數，執行時仍要有括弧，否則僅是代表函數這個『物件』。

```python
def func():
... print('Hello World!')
...
>>> func
<function func at 0x101a919d0>
>>> func()
12
```

## 巢狀函數 (Nesting Function)

函數內部可以再定義函數，稱為巢狀函數。

```python
>>> def function1():
...     def function2(s):
...         print(s)
...     function2('a')
...     function2('b')
... 
>>> function1()
a
b
```

## 遞迴函數 (Recursive Function)

函數是可以對自己呼叫的，這種自身呼叫的函數稱為遞迴函數。但這種條件的邏輯相當特殊，並不多見。

```python
# Python
def function_name():
    # ...
    function_name()
    # ...
```

遞迴函數最重要的是終止條件要確定，以確保能脫離遞迴，否則就是無窮迴圈，程式無法前進，只能強制關閉。
