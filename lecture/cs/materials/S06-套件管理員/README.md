# 套件管理員 (Package Manager)

各作業系統提供 console 的基本功能往往是不夠用的。常常會需要額外加裝套件補強。

因此，『補強套件的管理員』就因此被創造出來。[[參考]](https://en.wikipedia.org/wiki/List_of_software_package_management_systems)

## 優點

透過套件管理員，統一管理套件，可以有以下優點：

1. 統一管理，一目了然。
1. 一次性作業，包含安裝、更新、移除。
1. 掌握可能的相依套件 (dependency)。

## 軟體套件

傳統的套件管理，其種類、版本，支援性都大不相同。

### Unix-like

- apk
- apt
- yum

### macOS

- [Homebrew](https://brew.sh/)

### Windows

- [Chocolatey](https://chocolatey.org/)

### Application-Level

- Python
  - Conda
  - pip
- Node.js
  - npm
  - Bower
  - Yarn
- .Net
  - [NuGet](https://www.nuget.org/)
- Java
  - [Maven](https://maven.apache.org/)
  - Gradle

> 使用套件管理員，要詳閱文件說明，對於過期 (deprecated, expired) 或已經不再維護的專案，要避免使用。

## 軟體商店

透過網頁技術，商店形式的管理員也陸續推出。

### Android

- Google Play

### iOS

- App Store

### macOS

- App Store

### Windows

- Microsoft Store

### Platform

- Steam
- UPlay
