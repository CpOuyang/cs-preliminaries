# 數值 (Numeric)

數值，即對數字的紀錄。

## 整數 (Integer)

紀錄整數的方法，依照使用 byte 數的大小，可以定義出不同範圍的整數。下以 C# [文件](https://docs.microsoft.com/zh-tw/dotnet/csharp/language-reference/builtin-types/integral-numeric-types)來說明。

<img src="img/01.png" alt="drawing" width="600"/>

1. 使用越多的 Bytes 去定義一個數字，可用範圍就越大。

   > 夠用就好，而不是越大越好。

1. 如果要處理數字的正負，就會犧牲一個 bit 去記錄正負號。則：
   - 最大數值就會比『不處理正負號』時減半。&#10067;
   - 會浪費掉一種排列方式沒有用處。&#10071;&#10067;

1. 如果要處理的數值，是大到超過以上範圍的任意數。用傳統元件就無法操作，勢必要另外開發。

1. 計算機如果產生『超過可以承載範圍』的整數時，這個錯誤叫做 Overflow Error。

## 浮點數 (Floating-point Number)

對於處理非整數型態的數值，這在計算機發展初期就會遇到，當代工程師們提出的解決方式，是提出一種近似數值的型別：浮點數。

同樣以 C# [文件](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/floating-point-numeric-types)來說明。

<img src="img/02.png" alt="drawing" width="600"/>

1. 其中，

   長度為 32-bit 者稱為**單精度浮點數** (Single-precision Floating-point Number)；

   長度為 64-bit 者稱為**雙精度浮點數** (Double-precision Floating-point Number)。

1. 複習數學：任何數字可以以科學記號表示。

    ```math
    12345=1.2345_{10}\times10^4
    ```

   此例中 $`1.2345`$ 稱為有效數 (significand)，$`10`$ 為底數 (base)，$`4`$ 為指數 (exponent)。

1. 考量計算科學的實用性，科學記號表示法在實作上需要調整，以免間隙過大。為此 [IEEE 754](https://zh.wikipedia.org/wiki/IEEE_754) 也訂出相關標準。

   這裡的數學偏難，有興趣再延伸去研究即可。[示例](https://fabiensanglard.net/floating_point_visually_explained/)參考。

   大抵而言，數值還是可以拆成三個部分。

   <img src="img/03.png" alt="drawing" width="400"/>

   使得

   ```math
   \mathrm{Number}=(-1)^{\mathrm{Sign}}\times \mathrm{1.Fraction}_2\times2^{\mathrm{Exponent}-127}
   ```

   去表達任意非整數數值。

1. 從 C# 的官方文件來理解，浮點數還是有以下性質。

   - 投入的 Bytes 數越多，數字範圍越大。
   - 處理單一浮點數，隱含很多重的計算。

1. 任意數值，以浮點數表示是可能具有**誤差**的：有限的排列方式，無法表達無限多個數字。

   - 浮點數的運算，都會以(預先設定的)合理誤差，來表達預期數值。
   - 在誤差範圍內，計算機就會判斷二數為相同。&#10071;

   ```python
   >>> 1.1 == 1.01
   False
   >>> 1.0000000000000001 == 1.00000000000000001
   True
   ```
