# 字串 (String)

文字的實體儲存方式。無論是**程式碼**或**資料**，都會在硬碟實體儲存，供紀錄使用。

## ACSII

當計算機剛被發明時，最早期也最泛用的編碼方式。

- IEEE 於 1963 推出，後調整收錄在 ISO/IEC 646 國際標準。
- 是 7-bit 的字元編碼集合。
- 故每 Byte 的 first bit 固定為 `0`，有 $`2^7=128`$ 種編碼方式。

顯然，ASCII 不敷全世界各國使用。

## Big-5

台灣的電腦發展剛啟蒙階段，資策會邀集 13 廠商進行『五大軟體專案』(包含文書、資料庫、試算表、通訊、繪圖)。雖然專案並未成功，但編碼方式形成主流，成為業界標準，稱為大五碼，即後續指稱的繁體中文編碼。

- 1983 年版的 Big-5 包含 13,053 個中文字，及 441 個符號。
- 先按筆畫、後依部首排序。
- 1988 年定義出造字區，可增加 6,148 造字。
- 公務機關有利用造字區造字，稱為難字。
- 非官方則有倚天、微軟 Windows 3.1, 95, 98等版本造字。

結論：編碼紊亂，且還是不夠用。

## 其他地方

### 中港澳

- GBK (GB 13000)
- GB 18030

### 香港

- HKSCS

### 日本

- Shift JIS

### 中日韓

- CJK

## Unicode

源自加州，工程師們意識到國際編碼需要各國共同解決，1991 年組織了萬國碼聯盟 (The Unicode Consortium) ，吸收各國成員，包含企業，編排既有並整合創新編碼，時至今日，都有持續更新。[[參考]](https://home.unicode.org/)

> Unicode 是一種**編碼方式**；UTF 是一種實現 Unicode 的**實作方法**。

至 2021 年底，Unicode 就已經有近 15 萬種編碼方式。實作上會有以下問題：

- 表示至少需要 3 Bytes 才能完整呈現全貌。
- 一般書寫 ASCII 僅需要 1 Byte 即可 (全用 3 Bytes 會相當浪費)。
- 向下兼容 ASCII 才符合普世需要。
- 3 Bytes 不是 2 倍數。
- 3 Bytes 仍有上限。

## UTF-8

(使用 8-bit 為變動單位) 可變長度為 1 至 4 個 Bytes 的 Unicode 表示法。參考[[維基]](https://en.wikipedia.org/wiki/UTF-8) (中文版表格超過 Unicode 官方上限 U+10FFFF)。

<img src="img/01.png" alt="drawing" width="600"/>

## UTF-16

(使用 16-bit 為變動單位) 可變長度為 2 或 4 個 Bytes 的 Unicode 表示法。參考[[維基]](https://en.wikipedia.org/wiki/UTF-16)。

## UTF-32

(使用 32-bit 為變動單位) 固定長度 4 個 Bytes 的 Unicode 表示法。參考[[維基]](https://en.wikipedia.org/wiki/UTF-32)。

## BOM

[位元組順序記號](https://zh.wikipedia.org/wiki/位元組順序記號) (Byte-Order Mark)：在文檔開頭使用 2 至 4 Bytes 來說明這份文檔將以什麼方式 (通常是 Unicode) 編碼。

| Type | Hex of BOM |
|:-|:-|
| UTF-8 | `EF BB BF` |
| UTF-16 (BE) | `FE FF` |
| UTF-16 (LE) | `FF FE` |
| UTF-32 (BE) | `00 00 FE FF` |
| UTF-32 (LE) | `FF FE 00 00` |
| GB-18030 | `84 31 95 33` |

## Code Page

微軟自訂編碼表，來定義當前 console 或文件的編碼方式。詳細參考指令 `chcp`。

```batch
> help chcp
```

| Code Page | 編碼 |
|:-|:-|
| 936 | GBK 簡體中文 |
| 950 | Big-5 繁體中文 |
| 1250 | latin1 (ASCII) |
| 65001 | UTF-8 |

> 繁中語系開啟 cmd.exe 預設是 chcp 950。

## Python

Python 的編碼處理方式依大版本而不同。

- Python 2.x 版的編碼方式以各國語言為主，所以處理文檔或程式時，要依各地編碼而不同。

- Python 3.x 起編碼方式改以 Unicode，程式都以 Unicode 處理 (表示物件或變數命名皆然)，文檔的預設處理方式也是 Unicode。

```python
> # python 3
> # 這是壞習慣，請勿使用。以下只是示範。
> 變數 = 2
> print(變數*3)
6
```
