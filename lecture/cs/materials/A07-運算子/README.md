# 運算子 (Operator)

執行運算的符號，例如基本的四則運算符號，稱為**運算子**。

程式語言可以依照其特性及領域，實作不同的運算子，

## 算數運算 (Arithmetic Operation)

加法

```python
>>> # python
>>> 3 + 2
5
```

乘法

```python
>>> # python
>>> 3 * 2
6
```

## 邏輯運算 (Logical Operation)

(Logical) AND

```python
>>> # python
>>> True and False
False
```

(Logical) OR

```python
>>> # python
>>> True or False
True
```

(Logical) NOT

```python
>>> # python
>>> not False
True
```

## 位元間運算 (Bitwise Operation)

Bitwise Shifts

```python
>>> # python
>>> 4 << 2
16
```

```python
>>> # python
>>> 1024 >> 3
128
```

一個發展成熟的語言，例如 .Net Framework，都會有很豐富的運算子，強化實作上的彈性。
