# 物件導向程式設計 (OOP)

**物件導向程式設計** (**Object-Oriented Programming**; **OOP**) 是近代程式設計的基礎。將各種具體、抽象的概念或物件予以具象化，包裝後再延伸應用。近代的程式語言幾乎都會沿用 OOP 的機制加以擴展、推廣。

物件導向有四個重要的支柱。分別是：

- **抽象** (**Abstraction**)
- **封裝** (**Encapsulation**)
- **繼承** (**Inheritance**)
- **多型** (**Polymorphism**)

一般會用 class 來表達物件所泛指的『類別』，以下用 `animal` 這樣的類別來說明四種關係。

## 抽象 (Abstraction)

物件的資料 (屬性) 與函數 (方法)在實作方式，在使用時得以忽略，而只專注於它有哪些訊息，這種特性稱為抽象。

```c#
// c#
public class Animal
{
    private int date_of_birth { get; set; }
    public int age
    {
        get { return year_to_date(this.date_of_birth); }
    }
}
```

用圖示來表示，物件下有二個屬性。其中一為私有 (private) 屬性，一為公開 (public) 屬性。在某些語言裡 (不包含 Python)，private 表示不會被外界看到，只會在物件內部使用，public 表示可被外界看到，可以在物件之間使用。

```mermaid
classDiagram
    direction TB
    class Animal {
        -int date_of_birth
        +int age
    }
```

## 封裝 (Encapsulation)

將物件相關各種資料 (屬性) 與函數 (方法) 都包含在此物件之內，成為其特定成員，稱為封裝。

```c#
// c#
public class Animal
{
    private int date_of_birth { get; set; }
    public int age
    {
        get { return year_to_date(this.date_of_birth); }
    }
    // 學名
    public string sname { get; set; }
    public void eat()
    {
        Console.WriteLine("Eat something.");
    }
    public void run()
    {
        Console.WriteLine("Run away.");
    }
}
```

> `this` 是特殊保留字，在 C# 中表示『所在的物件本身』。

經此定義，物件 `Animal` 就更多了一個屬性 `sname`，與二個方法 `eat()` 與 `run()`。

```mermaid
classDiagram
    direction TB
    class Animal {
        -int date_of_birth
        +int age
        +string sname
        +eat()
        +run()
    }
```

## 繼承 (Inheritance)

物件可以向外延伸，定義更多的物件，稱為繼承。這種繼承性質，讓物件之間有了親代 (parent) 與子代 (child) 的關係。

```c#
// c#
public class Cat : Animal
{
    public void meow()
    {
        Console.WriteLine("Meow meow.");
    }
}

public class Dog : Animal
{
    public void bark()
    {
        Console.WriteLine("Woof woof.");
    }
}
```

繼承的物件，仍然保有親代的資料 (屬性) 與函數 (方法)。

```mermaid
classDiagram
    direction TB
    class Animal {
        -int date_of_birth
        +int age
        +string sname
        +eat()
        +run()
    }
    class Cat {
        -int date_of_birth
        +int age
        +string sname
        +eat()
        +run()
        +meow()
    }
    class Dog {
        -int date_of_birth
        +int age
        +string sname
        +eat()
        +run()
        +bark()
    }
    Animal --> Cat
    Animal --> Dog
```

## 多型 (Polymorphism)

相同名稱的資料 (屬性) 與函數 (方法)，可以在不同物件之間，有不同的實作方式，稱為多型。

```c#
// c#
public class Cat : Animal
{
    public override void run()
    {
        Console.WriteLine("Lick hair.");
        Console.WriteLine("Run away.");
    }
    public void meow()
    {
        Console.WriteLine("Meow meow.");
    }
}

public class Dog : Animal
{
    public override void run()
    {
        Console.WriteLine("Wag tail.");
        Console.WriteLine("Run away.");
    }
    public void bark()
    {
        Console.WriteLine("Woof woof.");
    }
}
```

由上可知，`run()` 函數 (方法)，在 `Cat` 與 `Dog` 之間的實作方式都不一樣，在親代的 `Animal` 身上也不同。

## 多重繼承 (Multiple Inheritance)

程式語言對 OOP 的支援程度不同，某些語言允許發生多重繼承。

> 『一對多』的繼承狀況幾乎都合法允許，也不大會發生問題。這裡的多重繼承，指的是『多對一』的繼承狀況。即單一物件繼承自多個親代物件。

```c#
// 這不是個合法的語句，僅供參考用。
public class Creature : Animal, Plant
```

多重繼承的發生狀況並不多見。除非在信心充足的前提下，才使用多重繼承，否則多重繼承容易發生混淆，應盡量避免。

| 允許多重繼承 | 不允許多重繼承 |
| :- | :- |
| C++ | Java |
| Python | C# |
