# 變數領域

變數的有效區域，會在一份程式碼中的位置不同而有所不同。

## 全域變數 (Global Variables)

在外層定義的變數，存活區域看以涵蓋整份程式碼者，稱為全域變數。

```text
>>> v1 = 1
>>> def func():
...     print(v1)
... 
>>> func()
1
```

上例中，變數 `v1` 可以在函數 `func` 中被呼叫使用。

## 區域變數 (Local Variables)

在函數間定義出來的變數，會在函數結束後被回收，無法在外層被利用，稱為區域變數。

```text
>>> def func():
...     v2 = 2
... 
>>> print(v2)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'v2' is not defined
```

<!-- 想要在函數內部處理全域變數，有二種做法。

**方法一：** 在全域空間先定義變數。

```text
>>> v3 = 3
>>> def func():
...     print(v3)
...
>>> func()
3
```

**方法二：** 在函數區間使用 `global` statement 定義全域變數。

```text
>>> v4 = 4
>>> def func():
...     global v4
...     print(v4)
...
>>> func()
4
``` -->

## 巢狀函數 (Nesting Function)

巢狀函數的生存區間與區域變數類似，在函數結束後，即會被回收而無法被外層使用。

```text
>>> def func1():
...     def func2():
...             v3 = 3
...     print(v3)
...
>>> func1()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<stdin>", line 4, in func1
NameError: name 'v3' is not defined
```

變數如此，函數亦同。

```text
>>> def func1():
...     def func2():
...             print('Hello World!')
...     func2()
...
>>> func1()
Hello World!
>>> func2()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'func2' is not defined
```
