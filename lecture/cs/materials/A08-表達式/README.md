# 表達式 (Expression)

表達式是指，任一程式語言在邏輯演算上的語句。實作的細部分類依各語言而不同，但大抵說來有幾種類型。

## Operation

使用運算子 (operators) 執行某種運算。

```python
>>> # python
>>> 3 + 2
5
>>> 3 ** 2
9
```

## Assignment

將運算結果**指派**給某變數或物件。

```python
# python
answer = 100
```

```python
# python
result = func(method='general')
```

## Statement

特殊功能的敘述句，完成某些運算或動作。

```python
# python
return result
```

```c#
// C#
int main()
```
