# 環境變數 (Environment Variable)

對執行中的進程而言 (process)，在記憶體配置動態 (dynamic)、可讀寫的變數，作為系統參數的設定，稱為環境變數。

## Windows

Windows 的設定指令為 `set`。

```batch
:: 設定環境變數
set NAME=value
```

若後面不加參數，則列出目前所有的環境變數。

```batch
:: 列出目前環境變數
set
```

引用環境變數，使用百分符號 (%) 前後包夾變數名稱，**不區分**大小寫 (case-insensitive)。

```batch
:: 列出使用者目錄
> echo %USERPROFILE%
C:\Users\savag
```

## Unix

Unix 的設定指令為 `export`。

```bash
# 設定環境變數
export NAME=value
```

同樣可列出所有環境變數。

```bash
# 列出目前環境變數
export
```

引用環境變數，使用貨幣符號 ($) 後皆變數名稱，**區分**大小寫 (case-sensitive)。

```bash
## 列出使用者目錄
$ echo $PWD
/users/charles
```

引用變數缺乏結尾符號，可以用括號 ("(" ... ")") 包覆變數名稱。
```bash
## 列出使用者目錄
$ echo $(USER)
charles
```

> $(...) 符號在 unix-like 作業系統下還有運算的功能，在 cli 環境下十分便利。

## PATH

PATH 是一個重要的環境變數。

各種程式的指令，除了所在資料夾，其次就會依次搜尋 PATH 所包含的路徑依序搜尋。

搜尋完後找不到可以套用的指令，就會出錯誤。

```batch
:: 新增路徑至 PATH，使用分號作分隔
set PATH=%PATH%;new_path_name
```

## 小結

1. 執行指令若出現找尋不到位置的錯誤，除了路徑是否正確、是否存在，再來就是要考量 PATH 有否包含。
1. 注意在單一進程中修改的環境變數，是會隨著進程的結束，而跟著結束。
1. 若要永久的修改環境變數：
   - 需要系統最高權限。
   - Windows 進入控制台修改。
   - Unix 修改 shell 啟動時的設定。
