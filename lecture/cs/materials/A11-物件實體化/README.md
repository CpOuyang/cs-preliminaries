# 物件實體化

有了物件的概念，自此以後，

- 物件的資料即稱為**屬性** (**Property**)。

- 物件的函數即稱為**方法** (**Method**)。

任何一種物件，都可由屬性與方法所組成。

## 實體化 (Instantiation)

程式執行時期，稱為 runtime。

1. 物件類別在完成定義後，會在記憶體佔有一席之地。

1. 物件類別在執行階段，需要一個動態的**實體** (**Instance**)。

   - 所謂的動態是指，要隨時紀錄類別運算的過程、結果，會因時而不同。

   - 製作實體的過程，稱為**實體化** (**Instantiation**)。程式語言會在記憶體區抄寫出另一片區域，供該實體使用。

```c#
// c#
using System;

class playClass
{
   static void Main (string[] args)
   {
      // 實體化
      Dog dog1 = new Dog();
      Dog dog2 = new Dog();
      Cat cat1 = new Cat();

      // 設定屬性
      dog1.sname = "拉布拉多";
      dog2.sname = "馬爾濟斯";
      cat1.sname = "米克斯";

      // 執行方法
      dog1.eat()
      cat1.run()
   }
}
```

## 靜態方法 vs 動態方法 (或屬性)

有些方法，其計算方式固定，使用到的元素不需要重新計算，其產出也不會因時而變化者，可以被定義為**靜態方法** (**Static Method**)。

靜態方法被定義後，不會改變，故不需要實體化即可使用。在使用時，需要以物件類別本身 (而不是新的實體) 為執行者，去呼叫該方法。

```c#
// c#
using System;

class playClass
{
   static void Main (string[] args)
   {
      Console.WriteLine(Math.PI);
      // 3.1415926535897931
   }
}
```

> 程式執行階段，稱為 runtime。

又例如 `Animal` 類別，每個實體在 runtime 都會有不同的 `age` 結果，這就是一種動態屬性。

```c#
// c#
using System;

class playClass
{
   static void Main (string[] args)
   {
      Dog dog = new Dog();

      Console.WriteLine(dog.age)
      // not implemented yet.
   }
}
```
