# Computer Science

[[_TOC_]]

## Generic

- [x] *[G01-程式語言分類](materials/G01-程式語言分類)
- [x] [G02-作業系統分類](materials/G02-作業系統分類)
- [x] [G03-整合開發環境](materials/G03-整合開發環境) IDE
- [ ] [G99-工具整理](materials/G99-工具整理) Toolings

## System

- [x] [S01-終端機](materials/S01-終端機) Terminal
- [x] [S02-殼語言](materials/S02-殼語言) Shell Language
- [x] [S03-標準串流](materials/S03-標準串流) Standard Streams
- [x] [S04-指令管道](materials/S04-指令管道) (Command) Pipe
- [x] [S05-環境變數](materials/S05-環境變數) Environment Variable
- [x] [S06-套件管理員](materials/S06-套件管理員) Package Manager
- [ ] 進程與線程 Process and Thread

## Program

### Basic

- [x] [A01-數值](materials/A01-數值)
- [x] [A02-字串](materials/A02-字串)
- [x] [A03-陣列](materials/A03-陣列) Array / List <!-- key-value pair -->
- [x] [A04-函數](materials/A04-函數) Function
- [ ] [A05-變數領域](materials/A05-變數領域)
- [ ] [A06-引數](materials/A06-引數) Argument
- [ ] [A07-運算子](materials/A07-運算子) Operator
- [ ] [A08-表達式](materials/A08-表達式) Expression
- [x] [A09-控制流程](materials/A09-控制流程) Control Flow
- [ ] [A10-物件導向程式設計](materials/A10-物件導向程式設計) OOP
- [ ] [A11-物件實體化](materials/A11-物件實體化)
- [ ] [A12-傳值與傳址](materials/A12-傳值與傳址)

### Advanced

- [ ] [B01-資源引用](materials/B01-資源引用)
- [ ] 正則表達式 Regular Expression
- [ ] 雜湊函數 Hash Function
- [ ] 匿名函數 Anonymous Function
- [ ] 錯誤處理 Error Handling
- [ ] 垃圾回收 Garbage Collection
- [ ] 單元測試 Unit Testing

## Network

- [ ] 網路七層 OSI Model
- [ ] 封包 Packet
- [ ] 通訊協定 Communication Protocol
- [ ] HTTP / HTTPS
- [ ] Request Methods
- [ ] 瀏覽器 Browsers
- [ ] IP / Port / DNS
- [ ] TCP/UDP
- [ ] SSH
- [ ] VPN

## Technique

- [ ] Markup Language
- [ ] Git, and providers
- [ ] RESTful API
- [ ] Virtualization <!-- https://www.intel.com.tw/content/www/tw/zh/support/articles/000005486/processors.html -->
- [ ] Docker, and containers
- [ ] Cloud, and services
- [ ] DevOps
