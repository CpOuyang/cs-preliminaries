# 科學記號 (Scientific Notation)

任何實數，都可以化為以下表達式

```math
a\times10^n
```

其中，

- $`a`$ 為實數，$`n`$ 為整數，且

- $`1\le\left|a\right|<10`$

## 實例

- $`3=3\times10^0`$

- $`65535=6.5535\times10^4`$

- $`0.00046=4.6\times10^{-4}`$

## 變形

科學記號的底數為 $`10`$ 時，常以 $`\mathrm{E}`$ 或 $`\mathrm{e}`$ 表示。

- $`6.5535\mathrm{E}4`$

- $`4.6\mathrm{E}-4`$

## 運算

有效率的算法，應是將實數部位相乘，再乘以指數部位相乘。

- $`\left(2\times10^3\right)\times\left(3\times10^2\right)\\=2\times10^3\times3\times10^2\\=2\times3\times10^3\times10^2\\=6\times10^5`$
