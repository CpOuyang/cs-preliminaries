# 古典機率 (Classic Probability)

古典機率論的發展，是從骰子賭博而來。

定義 $`S`$ 為所有發生可能性的事件空間，稱為母體；$`A`$ 為發生特定事件的集合；表示 $`A`$ 為 $`S`$ 的一部分，則 $`A`$ 發生的機率 $`P`$ 可以定義為

```math
P(A)=\frac{構成A的元素數目}{構成S的元素數目}
```

由此可知，機率越高，表示每次事件發生的可能性越高。

當 $`P(A)`=0$ 表示**必不會發生**，$`P(A)=1`$ 表示**必定會發生**。

## 實例

> 投擲銅板三次，三次都是正面的機率為何？

定義

```math
H=發生正面的事件\\
T=發生反面的事件
```

則 $`S`$ 可以視為三次投擲銅板的連續排列，可能結果共 $`2\times2\times2=8`$ 種，即

```math
\begin{align*}
S&=\{(H,H,H),\\
&~~~~~~~(T,H,H),(H,T,H),(H,H,T),\\
&~~~~~~~(T,T,H),(T,H,T),(H,T,T),\\
&~~~~~~~(T,T,T)\}
\end{align*}
```

而

```math
A=\{(H,H,H)\}
```

共 $`1`$ 種，因此

```math
P(A)=\frac{1}{8}=0.125
```

> 賭博遊戲『十八仔』要投擲四顆六面骰。倘若四顆皆不同，或三顆相同一顆不同時，需要重新骰過。請問每玩一次，要重骰的機率有多少？

投擲四顆六面骰的母體 $`S`$ 個數為 $`6^4=1296`$。則

```math
A=四顆皆不同\cup三顆相同一顆不同
```

其個數為

```math
\begin{align*}
&{6\choose1}{5\choose1}{4\choose1}{3\choose1}+{6\choose1}{1\choose1}{1\choose1}{5\choose1}\\\\
&=6\times5\times4\times3+6\times1\times1\times5\\
&=360+31\\
&=391
\end{align*}
```

則每次投擲後重骰機率為

```math
P(A)=\frac{391}{1296}=0.3017
```

> 一副撲克牌 $`52`$ 張，抽五張決勝。

1. 共有幾種抽法？

   ```math
   \begin{align*}
   &{52\choose5}=\frac{52\times51\times50\times49\times48}{5\times4\times3\times2\times1}\\
   &=\frac{311,875,200}{120}=2,598,960
   \end{align*}
   ```

1. 同花順 (Straight Flush) 排法有多少？

   ```math
   {4\choose1}{10\choose1}=4\times10=40
   ```

1. 四條 (Four of a Kind) 排法有多少？

   ```math
   {13\choose1}{48\choose1}=13\times48=624
   ```

1. 葫蘆 (Full House) 三條一對，排法有多少？

   ```math
   {13\choose1}{4\choose3}\times{12\choose1}{4\choose2}=13\times4\times12\times6=3,744
   ```

1. 同花 (Flush) 排法有多少？

   ```math
   \begin{align*}
   &{4\choose1}{13\choose5}-{4\choose1}{10\choose1}\\
   &=4\times\frac{13\times12\times11\times10\times9}{5\times4\times3\times2\times1}-4\times10\\
   &=4\times\frac{154,440}{120}-40\\
   &=5,108
   \end{align*}
   ```

1. 順子 (Straight) 排法有多少？

   ```math
   \begin{align*}
   &{10\choose1}{4\choose1}{4\choose1}{4\choose1}{4\choose1}{4\choose1}-{10\choose1}{4\choose1}\\
   &=10\times4^5-10\times4\\
   &=10,200
   \end{align*}
   ```

1. 三條 (Three of a Kind) 排法有多少？

   ```math
   \begin{align*}
   &{13\choose1}{4\choose3}\times{12\choose2}{4\choose1}{4\choose1}\\
   &=13\times4\times\frac{12\times11}{2\times1}\times4\times4\\
   &=54,912
   \end{align*}
   ```

1. 二對 (Two Pairs) 排法有多少？

   ```math
   \begin{align*}
   &{13\choose2}{4\choose2}{4\choose2}\times{11\choose1}{4\choose1}\\
   &=\frac{13\times12}{2\times1}\times\frac{4\times3}{2\times1}\times\frac{4\times3}{2\times1}\times11\times4\\
   &=123,552
   \end{align*}
   ```

1. 一對 (One Pair) 排法有多少？

   ```math
   \begin{align*}
   &{13\choose1}{4\choose2}\times{12\choose3}{4\choose1}{4\choose1}{4\choose1}\\
   &=13\times\frac{4\times3}{2\times1}\times\frac{12\times11\times10}{3\times2\times1}\times4\times4\times4\\
   &=1,098,240
   \end{align*}
   ```

1. 散牌 (No Pairs) 排法有多少？

   ```math
   全部排法-特殊牌面排法=1,302,540
   ```

1. 上述各種牌形機率各多少？

   ```math
   \begin{align*}
   P(同花順)&=\frac{40}{2,598,960}=1.54\times10^{-5}\\\\
   P(四條)&=\frac{624}{2,598,960}=0.00024\\\\
   P(葫蘆)&=\frac{3,744}{2,598,960}=0.00145\\\\
   P(同花)&=\frac{5,108}{2,598,960}=0.00197\\\\
   P(順子)&=\frac{10,200}{2,598,960}=0.00392\\\\
   P(二對)&=\frac{123,552}{2,598,960}=0.04754\\\\
   P(一對)&=\frac{1,098,240}{2,598,960}=0.42257\\\\
   P(散牌)&=\frac{1,302,540}{2,598,960}\\
   &=1-特殊牌面機率=0.50118
   \end{align*}
   ```

[[維基]](https://zh.wikipedia.org/wiki/撲克牌型)。

## 期望值 (Expected Value)

古典機率的倒數，是為古典機率的**期望值**。表示『事件發生的期望次數』。

- 『順子』的期望值為 $`0.00392^{-1}=255.1`$，表示抽牌 $`255.1`$ 次期望會發生事件一次。
- 『散牌』的期望值為 $`0.50118^{-1}=1.99`$，表示抽牌 $`1.99`$ 次期望會發生事件一次。
- 『散牌』的機率是『順子』的 $`0.50118\div0.00392=127.85`$ 倍，表示期望發生 $`127.85`$ 次『散牌』會發生一次『順子』。
