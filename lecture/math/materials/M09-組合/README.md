# 組合 (Combination)

將相異的物件作不同方式的拾取，拾取元素相同則視為相同，稱為組合。

## 問題

> 假設現有 4 張牌：A、B、C、D，抽取 3 張牌，相同牌組不計順序，試問有幾種組合法？

### 概念

```mermaid
flowchart TB;
    st{開始} --> n1(第一張牌有 4 種選擇)
    n1 --> n2(第二張牌有 3 種選擇)
    n2 --> n3(第三張牌有 2 種選擇)
    n3 --> n4(手上牌組多次排列視為一組)
    n4 --> en{結束}
```

因此，

```math
組合數(4,3)=\frac{4\times3\times2}{排列數(3,3)}=\frac{4\times3\times2}{3\times2\times1}=4
```

將**組合數**視為一函數，並計作 $`C`$。

而 $`C^n_m`$ 也經常寫作

```math
{n \choose m}
```

以方便閱讀。

## 公式

從 $`n`$ 個相異元素中取出 $`k`$ 的元素，其組合數為

```math
C^n_k=\frac{n!}{k!(n-k)!}
```

可整理得，

```math
\begin{equation*}
\begin{split}
C^n_k
&=\frac{n\times(n-1)\times\cdots\times(n-k)\times\cdots\times1}{k\times\cdots\times1\times(n-k)\times\cdots\times1}\\
&=\frac{n\times(n-1)\times\cdots\times(n-k+1)}{k\times\cdots\times1}
\end{split}
\end{equation*}
```

若**全部抽取**，即**全數組合**，此時 $`n=k`$，則，

```math
C^n_k=C^n_n=\frac{n!}{n!(n-n)!}=\frac{n!}{n!0!}=1
```

## 實例

1. [大樂透](https://www.taiwanlottery.com.tw/Lotto649/index.asp) 玩法從 $`1`$ 至 $`49`$ 號選 $`6`$ 個號碼進行投注...

   - 頭獎 (六個獎號完全相同) 有幾種買法？

     ```math
     C^6_6=\frac{6!}{0!6!}=1
     ```

   - 貳獎 (當期獎號任五碼＋特別號) 有幾種買法？

     ```math
     C^6_5C^1_1=\frac{6!}{1!5!}\times1=6
     ```

   - 參獎 (當期獎號任五碼) 有幾種買法？

     ```math
     C^6_5C^{42}_1=\frac{6!}{1!5!}\times42=6\times42=252
     ```

   - 肆獎 (當期獎號任四碼＋特別號) 有幾種買法？

     ```math
     C^6_4C^1_1C^{42}_1=\frac{6!}{2!4!}\times1\times42=15\times42=630
     ```

   - 伍獎 NT$2,000 (當期獎號任四碼) 有幾種買法？

     ```math
     C^6_4C^{42}_2=\frac{6!}{2!4!}\times\frac{42!}{2!40!}=15\times861=12915
     ```

   - 陸獎 NT$1,000 (當期獎號任三碼＋特別號) 有幾種買法？

     ```math
     C^6_3C^1_1C^{42}_2=\frac{6!}{3!3!}\times1\times\frac{42!}{2!40!}=20\times861=17220
     ```

   - 柒獎 NT$400 (當期獎號任兩碼＋特別號) 有幾種買法？
   - 普獎 NT$400 (當期獎號任三碼) 有幾種買法？
