# 排列 (Permutation)

將相異的物件作不同順序的排放，每一種順序都是為不同結果，稱為排列。

## 問題

> 假設現有 4 張牌：A、B、C、D，抽取 3 張牌，同一張牌順序不同視為相異，試問有幾種排列法？

### 概念

```mermaid
flowchart TB;
    st{開始} --> n1(第一張牌有 4 種選擇)
    n1 --> n2(第二張牌有 3 種選擇)
    n2 --> n3(第三張牌有 2 種選擇)
    n3 --> en{結束}
```

因此，

```math
排列數(4,3)=4\times3\times2=24
```

將**排列數**視為一函數，並計作 $`P`$。

## 公式

從 $`n`$ 個相異元素中取出 $`k`$ 的元素，其排列數為

```math
P^n_k=\frac{n!}{(n-k)!}
```

可整理得，

```math
\begin{equation*}
\begin{split}
P^n_k
&=\frac{n\times(n-1)\times\cdots\times(n-k)\times\cdots\times1}{(n-k)\times\cdots\times1}\\
&=n\times(n-1)\times\cdots\times(n-k+1)
\end{split}
\end{equation*}
```

若**全部抽取**，即**全數排列**，此時 $`n=k`$，則，

```math
P^n_k=P^n_n=\frac{n!}{(n-n)!}=\frac{n!}{0!}=n!
```

## 實例

1. 全班 $`20`$ 為學生，考取前 $`3`$ 名的排列數為何？

1. 一副撲克牌，隨意抽三張，越抽越大的排列數為何？
